require([
	'goo/entities/GooRunner',
	'goo/loaders/DynamicLoader',

	'goo/animation/layer/AnimationLayer',
	'goo/animation/state/SteadyState',
	'goo/animation/blendtree/ManagedTransformSource',
	'goo/animation/layer/LayerLERPBlender',

	'goo/entities/components/ScriptComponent',

	'goo/math/Vector3',
	'goo/math/Quaternion',
	'goo/math/Matrix3x3',
	'js/LookAtScript'
], function (
	GooRunner,
	DynamicLoader,

	AnimationLayer,
	SteadyState,
	ManagedTransformSource,
	LayerLERPBlender,

	ScriptComponent,

	Vector3,
	Quaternion,
	Matrix3x3,
	LookAtScript
) {
	'use strict';

	function init() {
		// Create typical Goo application
		var goo = new GooRunner({
			antialias: true,
			logo: false,
			manuallyStartGameLoop: true
		});

		// The loader takes care of loading the data
		var loader = new DynamicLoader({
			world: goo.world,
			rootPath: 'res'
		});

		loader.loadFromBundle('project.project', 'root.bundle', {recursive: false, preloadBinaries: true}).then(function(configs) {

			// This code will be called when the project has finished loading.
			var goon = loader.getCachedObjectForRef('bind_goon/entities/RootNode.entity');
			var clip = loader.getCachedObjectForRef('idle_a/animations/idle_a.clip');

			setupAnimationControl(goon, clip);
			setupSwitchIdle(goon);

			goo.renderer.domElement.id = 'goo';
			document.getElementById('goo-wrapper').appendChild(goo.renderer.domElement);

			// Start the rendering loop!
			goo.startGameLoop();

		})
	}
	/**
	 * Creates a {@link ManagedTransformSource} and a control script for looking
	 * at the mouse pointer
	 * @param {Entity} Entity
	 * @param {AnimationClip} clip
	 */
	function setupAnimationControl(entity, clip) {
		// A list of joints with names and weights in x and y direction
		var joints = [
			{
				name: 'BackA_M',
				x: 0.3,
				y: 0.5
			},
			{
				name: 'BackB_M',
				x: 0.3,
				y: 0.1
			},
			{
				name: 'Chest_M',
				x: 0.2,
				y: 0.1
			},
			{
				name: 'Neck_M',
				x: 0.2,
				y: 0.3
			}
		];
		var jointNames = joints.map(function(joint) { return joint.name; });

		// Add an animationlayer and get the source to control
		var clipSource = addManagedLayer(entity, clip, jointNames);

		// Add the controlScript
		entity.setComponent(
			new ScriptComponent(
				new LookAtScript({
					clipSource: clipSource,
					joints: joints,
					origo: [0,0.4,0]
				})
			)
		);

	}

	/**
	 * Creates an {@link AnimationLayer} with a playing {@link ManagedTransformSource}
	 * and adds it to the entity's animationComponent
	 * @param {Entity} entity
	 * @param {AnimationClip} clip
	 * @param {string[]} jointNames Names of jointChannels which the managedTransformSource
	 * will control
	 * @returns {ManagedTransformSource}
	 */
	function addManagedLayer(entity, clip, jointNames) {
		// Clipsource
		var clipSource = new ManagedTransformSource('Managed Clipsource');
		clipSource.initFromClip(clip, 'Include', jointNames);

		// State
		var state = new SteadyState('Managed State');
		state.setClipSource(clipSource);

		// Animation Layer
		var layer = new AnimationLayer('Managed Layer');
		layer.setState('managed', state);
		layer.setCurrentStateByName('managed');
		entity.animationComponent.addLayer(layer);

		return clipSource;
	}

	/**
	 * Set up a timer to mix up the idle animation a bit.
	 * @param {Entity} entity
	 */
	function setupSwitchIdle(entity) {
		// Will be called every 4 seconds
		var timer = function() {
			var rand = Math.random();
			var script = entity.scriptComponent.scripts[0];
			if (rand > 0.7 && !script.focus) {
				// Switch, then switch back
				entity.animationComponent.transitionTo('idle_b');
				setTimeout(function() {
					entity.animationComponent.transitionTo('idle_a');
				}, 1500);
			} else if (rand > 0.4 && !script.focus) {
				// Switch, then switch back
				entity.animationComponent.transitionTo('idle_c');
				setTimeout(function() {
					entity.animationComponent.transitionTo('idle_a');
				}, 1500);
			}
			setTimeout(timer, 4000);
		};
		timer();
	}

	init();
});
