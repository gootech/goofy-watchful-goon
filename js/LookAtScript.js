define([
	'goo/renderer/Renderer',

	'goo/math/Matrix3x3',
	'goo/math/Quaternion',
	'goo/math/Vector3',
	'goo/math/MathUtils'
], function(
	Renderer,

	Matrix3x3,
	Quaternion,
	Vector3,
	MathUtils
) {
	'use strict';
	/**
	 * @class A very non-generic script to control where a goon looks
	 * @param {object} properties
	 * @param {ManagedTransformSource} properties.clipSource
	 * @param {object[]} properties.joints
	 * @param {number[3]} properties.origo Where the eyes are in world coordinates
	 */
	function LookAtScript(properties) {
		properties = properties || {};
		this._blendWeight = 0;
		this._clipSource = properties.clipSource;
		this._joints = properties.joints;
		this._origo = new Vector3(properties.origo);
		this._azimuth = 0;
		this._ascent = 0;
		this._domElement = null;
		this._dirty = true;
		this._timeout = null;
		this._loseFocus = this._loseFocus.bind(this);

		/** @type {boolean} */
		this.focus = false;
	}

	// Calculation helpers (creating objects is heavy)
	var calcMat = new Matrix3x3();
	var calcQuat = new Quaternion();

	/**
	 * Called by Goo Engine, will be executed every render loop
	 * @param {Entity} entity
	 * @param {number} tpf time per frame
	 * @param {object} env
	 * @param {DOMElement} env.domElement The canvas Goo is running on
	 */
	LookAtScript.prototype.run = function(entity, tpf, env) {
		// Setup events
		if (env) {
			if (!this._domElement && env.domElement) {
				this._domElement = env.domElement;
				this._setupMouseControls();
			}
		}
		// Blend the goons gaze to look at mouse or look back
		if (this.focus) {
			// Look at mouse
			if (this._blendWeight > 0.98) {
				this._blendWeight = 1;
			} else {
				this._blendWeight = 1 - (1 - this._blendWeight)*0.9;
			}
		} else {
			// Back to idling
			if (this._blendWeight < 0.002) {
				this._blendWeight = 0;
			} else {
				this._blendWeight *= 0.97;
			}
		}
		entity.animationComponent.layers[1].setBlendWeight(this._blendWeight);
		
		if (this._clipSource && this._joints && this._dirty) {
			var source = this._clipSource;
			// Splitting up the angles on the joints according to weights
			for (var i = 0, len = this._joints.length; i < len; i++) {
				var joint = this._joints[i];
				calcMat.fromAngles(this._azimuth * joint.x, 0, this._ascent * joint.y);
				calcQuat.fromRotationMatrix(calcMat);
				this._clipSource.setRotation(joint.name, calcQuat);
			}
			this._dirty = false;
		}
	};
	
	// Calculation helper
	var calcVec = new Vector3();

	/**
	 * Sets up the mouse controls, calculates the angles and stores them
	 * to be used later in render loop
	 */
	LookAtScript.prototype._setupMouseControls = function() {
		// If we move the mouse, get where the goon should look
		document.addEventListener('mousemove', function(e) {
			var rect = this._domElement.getBoundingClientRect();
			this.focus = true;
			var camera = Renderer.mainCamera;
			if (camera) {
				// Screen coordinates in canvas space
				var x = e.pageX - rect.left;
				var y = e.pageY - rect.top;

				// World coordinates slightly in front of the camera
				camera.getWorldCoordinates(
					x,
					y,
					rect.width,
					rect.height,
					0.01,
					calcVec
				);
				calcVec.subv(this._origo);

				// Get the angles
				this._ascent = Math.atan2(calcVec.y, calcVec.z);
				this._azimuth = Math.atan2(calcVec.x, calcVec.z);

				this._dirty = true;
				
				// Stay focused a bit longer
				clearTimeout(this._timeout);
				this._timeout = setTimeout(this._loseFocus, 2000);
				
			}
		}.bind(this));
	};
	
	/**
	 * Makes goon lose focus and go back to idling
	 */
	LookAtScript.prototype._loseFocus = function() {
		this.focus = false;
	};
	
	return LookAtScript
});